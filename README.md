# VICOS Manager

Esta apliação foi desenvolvida utilizando o [CakePHP](http://cakephp.org) 3.x. 

O VICOS Manager está sendo mantido no Bitbucket e pode ser acessado aqui: [Bitbucket](https://bitbucket.org/androrim/vicosmanager).

## Instalação

* Descompacte no diretório que melho lhe convier.
* No seu terminal, navegue até a raiz deste projeto.
* Rode `composer install`.

You should now be able to visit the path to where you installed the app and see the default home page.

## Configuração

* No arquivo `config/app.php` altere `Datasources` para as configurações de seu banco de dados.

* **IMPORTANTE**: não altere no nome do banco de dados em `Datasources.default.database`. O valor de `Datasources.default.database` deve ser `vic_os_manager`.

* Importe para seu banco de dados o arquivo `Dump.sql` localizado na raiz do deste projeto;

## Rodar a Aplicação

No seu terminal, navegue até a raiz deste projeto.

### Servidor Web Embutido PHP
```
php -S localhost:3000 -t webroot/
```

### Servidor Web Embutido CakePHP
```
bin/cake server -H localhost -p 3000
```

Pronto o projeto estará rodando em [http://localhost:3000](http://localhost:3000)