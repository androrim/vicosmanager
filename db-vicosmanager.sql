
CREATE DATABASE IF NOT EXISTS vic_os_manager
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE vic_os_manager;

CREATE TABLE IF NOT EXISTS categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci
);

CREATE TABLE IF NOT EXISTS services (
    id INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci,
    price VARCHAR(255),
    deadline INT(2)
);

CREATE TABLE IF NOT EXISTS persons (
    cpf VARCHAR(11) PRIMARY KEY UNIQUE NOT NULL,
    name VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    email VARCHAR(60) NOT NULL,
    category_id INT(1) NOT NULL,
    FOREIGN KEY category_key (category_id) REFERENCES categories(id)
);

CREATE TABLE IF NOT EXISTS so (
    id INT AUTO_INCREMENT PRIMARY KEY,
    executor_id VARCHAR(11),
    client_id VARCHAR(11),
    status INT(1),
    date DATETIME DEFAULT NOW(),
    FOREIGN KEY executor_key (executor_id) REFERENCES persons(cpf),
    FOREIGN KEY client_key (client_id) REFERENCES persons(cpf)
);

CREATE TABLE IF NOT EXISTS so_services (
    id INT AUTO_INCREMENT PRIMARY KEY,
    so_id INT,
    service_id INT,
    FOREIGN KEY so_key (so_id) REFERENCES so(id),
    FOREIGN KEY service_key (service_id) REFERENCES services(id)
);

INSERT INTO categories (description) VALUES ('Executor');
INSERT INTO categories (description) VALUES ('Client');