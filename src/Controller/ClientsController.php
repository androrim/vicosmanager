<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\PersonsTable $Persons
 */
class ClientsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->set('title', 'Clients');
    }

    public function index()
    {
        $where = ['category_id' => 2];
        $s = null;

        if (isset($_GET['s']) ) {
            $s = $_GET['s'];
            $where['name LIKE'] = '%' . $s . '%';
        }

        $query = $this->loadModel('Persons')->find('all')->where($where);

        $clients = $this->paginate($query);

        $this->set(compact('clients', 's'));
    }

    public function view($id = null)
    {
        $client = $this->loadModel('Persons')->get($id, [
            'contain' => ['Categories']
        ]);

        $soList = $this->loadModel('So')->find('all')->where([
            'client_id =' => $client->cpf
        ])->toArray();

        $this->set(compact('client', 'soList'));
    }

    public function add()
    {
        $client = $this->loadModel('Persons')->newEntity();

        if ($this->request->is('post')) {

            $client = $this->loadModel('Persons')->patchEntity($client, $this->request->data);
            $saved = $this->loadModel('Persons')->save($client);
            
            if ($saved) {
                $this->Flash->success(__('The client has been saved.'));
                return $this->redirect(['action' => 'view/' . $saved->cpf]);
            } 
            else {
                foreach ($client->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }

                $this->set(compact('client'));
                return;
            }
        }

        $this->set(compact('client'));
    }

    public function edit($id = null)
    {
        $client = $this->loadModel('Persons')->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $client = $this->loadModel('Persons')->patchEntity($client, $this->request->data);

            if ($this->loadModel('Persons')->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'view/' . $client->cpf]);
            } 
            else {
                foreach ($client->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }

                $this->set(compact('client'));
                return;
            }
        }

        $this->set(compact('client'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->loadModel('Persons')->get($id);
        
        $soQuery = $this->loadModel('So')->find('all')->where([
            'client_id =' => $client->cpf
        ]);

        if (count($soQuery->toArray()) > 0) {
            $this->Flash->error(__('The client could not be deleted. He\'s related be one or more Services Orders.'));
            return $this->redirect(['action' => 'view/' . $client->cpf]);
        }


        if ($this->loadModel('Persons')->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
