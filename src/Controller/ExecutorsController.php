<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Executors Controller
 *
 * @property \App\Model\Table\PersonsTable $Persons
 */
class ExecutorsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->set('title', 'Executors');
    }

    public function index()
    {
        $where = ['category_id' => 1];
        $s = null;

        if (isset($_GET['s']) ) {
            $s = $_GET['s'];
            $where['name LIKE'] = '%' . $s . '%';
        }

        $query = $this->loadModel('Persons')->find('all')->where($where);

        $executors = $this->paginate($query);

        $this->set(compact('executors', 's'));
    }

    public function view($id = null)
    {
        $executor = $this->loadModel('Persons')->get($id, [
            'contain' => ['Categories']
        ]);

        $soList = $this->loadModel('So')->find('all')->where([
            'executor_id =' => $executor->cpf
        ])->toArray();

        $this->set(compact('executor', 'soList'));
    }

    public function add()
    {
        $executor = $this->loadModel('Persons')->newEntity();

        if ($this->request->is('post')) {
            $executor = $this->loadModel('Persons')->patchEntity($executor, $this->request->data);
            $saved = $this->loadModel('Persons')->save($executor);
            
            if ($saved) {
                $this->Flash->success(__('The executor has been saved.'));
                return $this->redirect(['action' => 'view/' . $saved->cpf]);
            } 
            else {
                foreach ($executor->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }

                $this->set(compact('executor'));
                return;
            }
        }

        $this->set(compact('executor'));
    }

    public function edit($id = null)
    {
        $executor = $this->loadModel('Persons')->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $executor = $this->loadModel('Persons')->patchEntity($executor, $this->request->data);

            if ($this->loadModel('Persons')->save($executor)) {
                $this->Flash->success(__('The executor has been saved.'));

                return $this->redirect(['action' => 'view/' . $executor->cpf]);
            } 
            else {
                foreach ($executor->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }

                $this->set(compact('executor'));
                return;
            }
        }

        $this->set(compact('executor'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $executor = $this->loadModel('Persons')->get($id);

        $soQuery = $this->loadModel('So')->find('all')->where([
            'executor_id =' => $executor->cpf
        ]);

        if (count($soQuery->toArray()) > 0) {
            $this->Flash->error(__('The executor could not be deleted. He\'s related be one or more Services Orders.'));
            return $this->redirect(['action' => 'view/' . $executor->cpf]);
        }


        if ($this->loadModel('Persons')->delete($executor)) {
            $this->Flash->success(__('The executor has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The executor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
