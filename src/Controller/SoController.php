<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;


/**
 * So Controller
 *
 * @property \App\Model\Table\SoTable $So
 */
class SoController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->set('title', 'Services Orders');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $So = $this->So;
        $s = null;
        $di = null;
        $df = null;

        if (isset($_GET['s']) && isset($_GET['di']) && isset($_GET['df'])) {
            
            $s = $_GET['s'];
            $di = new Time($_GET['di']);
            $df = new Time($_GET['df']);

            // $So = $this->So->find('all', [
            //     'contain' => [
            //         'Services' => [
            //             'conditions' => ['description LIKE' => '%' . $s . '%']
            //         ]
            //     ],
            //     'conditions' => [
            //         'date >=' => $di->i18nFormat('yyyy-MM-dd HH:mm:ss'),
            //         'date <=' => $df->i18nFormat('yyyy-MM-dd HH:mm:ss')
            //     ]
            // ]);

            // !!!BIGODE!!!
            $So = $this->So->find('all')->where([
                'date >=' => $di->i18nFormat('yyyy-MM-dd HH:mm:ss'),
                'date <=' => $df->i18nFormat('yyyy-MM-dd HH:mm:ss'),
                'description LIKE' => '%' . $s . '%'
            ])->join([
                'table' => 'so_services',
                'type' => 'LEFT',
                'conditions' => [
                    'so_services.so_id = So.id',
                ]
            ])->join([
                'table' => 'services',
                'type' => 'LEFT',
                'conditions' => [
                    'services.id = so_services.service_id',
                ]
            ]);

        }
  
        $so     = $this->paginate($So);
        $self   = $this;

        $this->set(compact('so', 'self', 's', 'di', 'df'));
    }

    public function view($id = null)
    {
        $so = $this->So->get($id,[
            'contain' => ['Services']
        ]);

        $so->executor   = $this->getExecutor($so);
        $so->client     = $this->getClient($so);
        $status         = $this->getStatus($so->status);

        $this->set(compact('so', 'status'));
    }

    public function add()
    {
        $so = $this->So->newEntity();

        if ($this->request->is('post')) {

            $this->request->data['date'] = new Time($this->request->data['date']);
            $so = $this->So->patchEntity($so, $this->request->data);
            $saved = $this->So->save($so);
            
            if ($saved) {
                $this->Flash->success(__('The so has been saved.'));
                return $this->redirect(['action' => 'view/' . $saved->id]);
            } 
            else {
                foreach ($so->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }
            }
        }
        
        $clients    = $this->So->Persons->find('all')->where(['category_id =' => 2]);
        $executors  = $this->So->Persons->find('all')->where(['category_id =' => 1]);
        $status     = $this->getStatus();
        $services   = $this->So->Services->find('all');

        if (count($services->toArray()) == 0) {
            $this->Flash->warning(__('ATENTION! You do not have Services. Create a Service before.'));
        }

        if (count($clients->toArray()) == 0) {
            $this->Flash->warning(__('ATENTION! You do not have Clients. Create a Client before.'));
        }

        if (count($executors->toArray()) == 0) {
            $this->Flash->warning(__('ATENTION! You do not have Executors. Create a Executor before.'));
        }

        $this->set(compact('so', 'clients', 'executors', 'status', 'services'));
    }

    /**
     * Edit method
     *
     * @param string|null $id So id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $so = $this->So->get($id,[
            'contain' => ['Services']
        ]);

        if ($so->status > 0) {
            return $this->redirect(['action' => 'view/' . $id]);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['date'] = new Time($this->request->data['date']);
            $so = $this->So->patchEntity($so, $this->request->data);

            if ($this->So->save($so)) {
                $this->Flash->success(__('The so has been saved.'));
                
                if ($so->status > 0) {
                    return $this->redirect(['action' => 'view/' . $id]);
                }
            } 
            else {
                foreach ($so->errors() as $error) {
                    foreach ($error as $msg) {
                        $this->Flash->error($msg);
                    }
                }
            }
        }

        $clients    = $this->So->Persons->find('all')->where(['category_id =' => 2]);
        $executors  = $this->So->Persons->find('all')->where(['category_id =' => 1]);
        $services   = $this->So->Services->find('all'); 
        $status     = $this->getStatus();
        
        foreach($services as $service) {
            $service->selected = '';

            foreach($so->services as $soService) {
                if ($soService->id == $service->id) {
                    $service->selected = 'selected';
                }
            }
        }
        
        $this->set(compact('so', 'executors', 'clients', 'status', 'services'));
    }

    /**
     * Delete method
     *
     * @param string|null $id So id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $so = $this->So->get($id);
        
        if ($this->So->delete($so)) {
            $this->Flash->success(__('The so has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The so could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getClient($so) {
        return $this->So->Persons->find('all')->where(['cpf =' => $so->client_id])->first();
    }

    public function getExecutor($so) {
        return $this->So->Persons->find('all')->where(['cpf =' => $so->executor_id])->first();
    }

    public function getStatus($id = null) 
    {
        $status = [0 => 'Created', 1 => 'Started', 2 => 'Completed'];

        if (is_null($id)) {
            return $status;
        }

        return $status[$id];
    }


}
