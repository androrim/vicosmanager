<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SoServices Controller
 *
 * @property \App\Model\Table\SoServicesTable $SoServices
 */
class SoServicesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['So', 'Services']
        ];
        $soServices = $this->paginate($this->SoServices);

        $this->set(compact('soServices'));
        $this->set('_serialize', ['soServices']);
    }

    /**
     * View method
     *
     * @param string|null $id So Service id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $soService = $this->SoServices->get($id, [
            'contain' => ['So', 'Services']
        ]);

        $this->set('soService', $soService);
        $this->set('_serialize', ['soService']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $soService = $this->SoServices->newEntity();
        if ($this->request->is('post')) {
            $soService = $this->SoServices->patchEntity($soService, $this->request->data);
            if ($this->SoServices->save($soService)) {
                $this->Flash->success(__('The so service has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The so service could not be saved. Please, try again.'));
            }
        }
        $so = $this->SoServices->So->find('list', ['limit' => 200]);
        $services = $this->SoServices->Services->find('list', ['limit' => 200]);
        $this->set(compact('soService', 'so', 'services'));
        $this->set('_serialize', ['soService']);
    }

    /**
     * Edit method
     *
     * @param string|null $id So Service id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $soService = $this->SoServices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $soService = $this->SoServices->patchEntity($soService, $this->request->data);
            if ($this->SoServices->save($soService)) {
                $this->Flash->success(__('The so service has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The so service could not be saved. Please, try again.'));
            }
        }
        $so = $this->SoServices->So->find('list', ['limit' => 200]);
        $services = $this->SoServices->Services->find('list', ['limit' => 200]);
        $this->set(compact('soService', 'so', 'services'));
        $this->set('_serialize', ['soService']);
    }

    /**
     * Delete method
     *
     * @param string|null $id So Service id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $soService = $this->SoServices->get($id);
        if ($this->SoServices->delete($soService)) {
            $this->Flash->success(__('The so service has been deleted.'));
        } else {
            $this->Flash->error(__('The so service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
