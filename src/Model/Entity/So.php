<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * So Entity
 *
 * @property int $id
 * @property string $executor_id
 * @property string $client_id
 * @property int $status
 * @property \Cake\I18n\Time $date
 *
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\Service[] $services
 */
class So extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
