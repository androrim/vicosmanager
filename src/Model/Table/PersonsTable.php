<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Persons Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \App\Model\Entity\Person get($primaryKey, $options = [])
 * @method \App\Model\Entity\Person newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Person[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Person|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Person patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Person[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Person findOrCreate($search, callable $callback = null)
 */
class PersonsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('persons');
        $this->displayField('name');
        $this->primaryKey('cpf');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('cpf', 'create')
            ->notEmpty('cpf', 'CPF can\'t be empty.')
            ->add('cpf', 'cpf', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'This CPF already registered.'])
            ->add('cpf', 'cpf-length', ['rule' => ['minLength', 11], 'message' => 'CPF must be 11 characters long.']);

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'Name can\'t be empty.')
            ->add('name', 'name-length', ['rule' => ['minLength', 3], 'message' => 'Name require minimum 3 characters.']);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email', 'Email can\'t be empty.')
            ->add('email', 'email-valid', ['rule' => 'email', 'message' => 'Valid email required.'])
            ->add('email', 'email', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'This Email already registered.']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['cpf']));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
