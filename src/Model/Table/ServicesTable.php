<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $So
 *
 * @method \App\Model\Entity\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, callable $callback = null)
 */
class ServicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('services');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('So', [
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'so_id',
            'joinTable' => 'so_services'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('description')
            ->notEmpty('description', 'Description can\'t be empty.')
            ->add('description', 'description-length', ['rule' => ['minLength', 10], 'message' => 'Description must be 10 characters long.']);

        $validator
            ->allowEmpty('price')
            ->notEmpty('price', 'Price can\'t be empty.')
            ->add('price', 'price-numeric', ['rule' => function ($data, $provider) {
                return is_numeric($data);
            }, 'message' => 'Price must be numeric.']);

        $validator
            ->allowEmpty('deadline')
            ->notEmpty('deadline', 'Deadline can\'t be empty.')
            ->add('deadline', 'deadline-numeric', ['rule' => function ($data, $provider) {
                return is_numeric($data);
            }, 'message' => 'Deadline must be numeric.']);

        return $validator;
    }
}
