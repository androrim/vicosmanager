<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SoServices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $So
 * @property \Cake\ORM\Association\BelongsTo $Services
 *
 * @method \App\Model\Entity\SoService get($primaryKey, $options = [])
 * @method \App\Model\Entity\SoService newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SoService[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SoService|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SoService patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SoService[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SoService findOrCreate($search, callable $callback = null)
 */
class SoServicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('so_services');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('So', [
            'foreignKey' => 'so_id'
        ]);
        $this->belongsTo('Services', [
            'foreignKey' => 'service_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['so_id'], 'So'));
        $rules->add($rules->existsIn(['service_id'], 'Services'));

        return $rules;
    }
}
