<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * So Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Persons
 * @property \Cake\ORM\Association\BelongsTo $Persons
 * @property \Cake\ORM\Association\BelongsToMany $Services
 *
 * @method \App\Model\Entity\So get($primaryKey, $options = [])
 * @method \App\Model\Entity\So newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\So[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\So|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\So patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\So[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\So findOrCreate($search, callable $callback = null)
 */
class SoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('so');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Persons', [
            'foreignKey' => 'executor_id'
        ]);
        $this->belongsTo('Persons', [
            'foreignKey' => 'client_id'
        ]);
        $this->belongsToMany('Services', [
            'foreignKey' => 'so_id',
            'targetForeignKey' => 'service_id',
            'joinTable' => 'so_services'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->dateTime('date')
            ->allowEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['executor_id'], 'Persons'));
        $rules->add($rules->existsIn(['client_id'], 'Persons'));

        return $rules;
    }
}
