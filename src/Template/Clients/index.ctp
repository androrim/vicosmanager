
<h3><?php echo __('Clients') ?></h3>

<hr />

<?php if (count($clients->toArray()) > 0) :  ?>

<?php echo $this->element('simple-search-form', [
    'controller' => 'Clients',
    'placeholder' => 'Search by client name',
    's' => $s,
    'items' => $clients
]) ?>

    
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th scope="col" class="text-uppercase"><?php echo $this->Paginator->sort('cpf') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('name') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('email') ?></th>
            <th scope="col" class="actions"><?php echo __('Actions') ?></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($clients as $client): ?>
        <tr>
            <td><?php echo h($client->cpf) ?></td>
            <td><?php echo h($client->name) ?></td>
            <td><?php echo h($client->email) ?></td>
            <td class="actions">
                <?php echo $this->Html->link(__(''), ['action' => 'view', $client->cpf, 'controller' => 'Clients'], ['class' => 'glyphicon glyphicon-eye-open']) ?>
                <?php echo $this->Html->link(__(''), ['action' => 'edit', $client->cpf, 'controller' => 'Clients'], ['class' => 'glyphicon glyphicon-pencil']) ?>
                <?php echo $this->Form->postLink(
                    __(''), 
                    ['action' => 'delete', $client->cpf, 'controller' => 'Clients'], 
                    ['confirm' => __('Are you sure you want to delete # {0}?', $client->cpf), 
                        'class' => 'glyphicon glyphicon-trash']
                ) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('paginator') ?>

<?php else : ?>
<p><?php echo __('You do not have Clients to list') ?>. 
<?php echo $this->Html->link(__('Add client'), ['controller' => 'Clients', 'action' => 'add']) ?></p>
<?php endif ?>
