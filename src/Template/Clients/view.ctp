
<h3 class="pull-left"><?php echo h($client->name) ?></h3>

<?php 
echo $this->Form->postLink( 
    __('Delete'), 
    ['action' => 'delete', $client->cpf],  
    ['confirm' => __('Are you sure you want to delete # {0}?', $client->cpf), 
        'class' => 'btn btn-danger pull-right']);
?>

<div class="clearfix"></div>

<hr />
    
<table class="vertical-table">
    <tr>
        <th scope="row"><?php echo __('CPF') ?></th>
        <td><?php echo h($client->cpf) ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Name') ?></th>
        <td><?php echo h($client->name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Email') ?></th>
        <td><?php echo h($client->email) ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Category') ?></th>
        <td><?php echo $client->has('category') ? h($client->category->description) : '' ?></td>
    </tr>
</table>

<h4><?php echo __('Services Orders') ?></h4>

<div class="row">
    <div class="col-md-9">
        <?php if(count($soList) > 0) : ?>
        <?php foreach($soList as $so) : ?>
        <?php 
        echo $this->Html->link(
            'Id: ' . $so->id . ' - Date: ' . date_format($so->date, 'm/d/Y'), 
            ['controller' => 'So', 'action' => 'view', $so->id], 
            ['class' => 'label label-default']);
        ?>
        <?php endforeach ?>
        <?php else : ?>
        <p><?php echo __('This client is\'nt related any Service Order') ?>.</p>
        <?php endif ?>
    </div>
</div>
