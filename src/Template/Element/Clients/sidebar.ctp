
<nav class="sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->request->action == 'index' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('List Clients'), ['controller' => 'Clients', 'action' => 'index']) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'add' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Add Client'), ['controller' => 'Clients', 'action' => 'add']) ?>
        </li>
        <?php if ($this->request->action == 'view' || $this->request->action == 'edit') : ?>
        <li role="presentation" class="<?php echo $this->request->action == 'view' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('View Client'), ['controller' => 'Clients', 'action' => 'view', $client->cpf]) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'edit' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Edit Client'), ['controller' => 'Clients', 'action' => 'edit', $client->cpf]) ?>
        </li>
        <?php endif ?>
    </ul>
</nav>