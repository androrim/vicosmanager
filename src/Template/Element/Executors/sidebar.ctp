
<nav class="sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->request->action == 'index' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('List Executors'), ['controller' => 'Executors', 'action' => 'index']) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'add' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Add Executor'), ['controller' => 'Executors', 'action' => 'add']) ?>
        </li>
        <?php if ($this->request->action == 'view' || $this->request->action == 'edit') : ?>
        <li role="presentation" class="<?php echo $this->request->action == 'view' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('View Executor'), ['controller' => 'Executors', 'action' => 'view', $executor->cpf]) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'edit' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Edit Executor'), ['controller' => 'Executors', 'action' => 'edit', $executor->cpf]) ?>
        </li>
        <?php endif ?>
    </ul>
</nav>