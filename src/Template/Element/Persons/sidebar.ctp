
<nav class="sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->request->action == 'index' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('List Executors'), ['controller' => 'Executors', 'action' => 'index']) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'add' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Add Executors'), ['controller' => 'Persons', 'action' => 'add']) ?>
        </li>
        <?php if ($this->request->action == 'view' || $this->request->action == 'edit') : ?>
        <li role="presentation" class="<?php echo $this->request->action == 'view' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('View Executors'), ['controller' => 'Executors', 'action' => 'view', $person->id]) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'edit' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Edit Executors'), ['controller' => 'Executors', 'action' => 'edit', $person->id]) ?>
        </li>
        <?php endif ?>
    </ul>
</nav>