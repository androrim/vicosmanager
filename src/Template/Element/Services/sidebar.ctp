
<nav class="sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->request->action == 'index' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('List Sercices'), ['controller' => 'Services', 'action' => 'index']) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'add' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Add Service'), ['controller' => 'Services', 'action' => 'add']) ?>
        </li>
        <?php if ($this->request->action == 'view' || $this->request->action == 'edit') : ?>
        <li role="presentation" class="<?php echo $this->request->action == 'view' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('View Service'), ['controller' => 'Services', 'action' => 'view', $service->id]) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'edit' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Edit Service'), ['controller' => 'Services', 'action' => 'edit', $service->id]) ?>
        </li>
        <?php endif ?>
    </ul>
</nav>