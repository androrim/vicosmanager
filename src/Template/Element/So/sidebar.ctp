
<nav class="sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->request->action == 'index' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('List Sercices Orders'), ['controller' => 'So', 'action' => 'index']) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'add' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Add Service Order'), ['controller' => 'So', 'action' => 'add']) ?>
        </li>
        <?php if ($this->request->action == 'view' || $this->request->action == 'edit') : ?>
        <li role="presentation" class="<?php echo $this->request->action == 'view' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('View Service Order'), ['controller' => 'So', 'action' => 'view', $so->id]) ?>
        </li>
        <li role="presentation" class="<?php echo $this->request->action == 'edit' ? 'active' : '' ?>">
            <?php echo $this->Html->link(__('Edit Service Order'), ['controller' => 'So', 'action' => 'edit', $so->id]) ?>
        </li>
        <?php endif ?>
    </ul>
</nav>