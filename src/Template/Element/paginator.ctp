<div class="paginator">
    <ul class="pagination">
        <?php echo $this->Paginator->prev('<<') ?>
        <?php echo $this->Paginator->numbers() ?>
        <?php echo $this->Paginator->next('>>') ?>
    </ul>
    
    <p><?php echo $this->Paginator->counter() ?></p>
</div>