
<?php echo $this->Form->create($controller, array('type' => 'get', 'class' => 'search-form form-group')); ?>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <input class="form-control" type="text" name="s" placeholder="<?php echo $placeholder ?>">
        </div>
    </div>

    <div class="col-lg-6">
        <button type="submit" class="btn btn-default navbar-btn"><?php echo __('Search') ?></button>
    </div>
</div>
<?php echo $this->Form->end() ?>

<?php if (!is_null($s)) : ?>
<div class="well well-sm">
    <?php echo count($items) ?>
    <?php echo __('Results for') ?> 
    <i>"<?php echo $s; ?>"</i>
    
    <?php 
    $span = $this->Html->tag('span', [
        'class' => 'glyphicon glyphicon-remove'
    ]);

    echo $this->Html->link(__(''),
        ['controller' => $controller, 'action' => 'index'], 
        ['class' => 'pull-right btn btn-default btn-xs glyphicon glyphicon-erase',
            'title' => 'Clear']);
    ?>
</div>
<?php endif ?>