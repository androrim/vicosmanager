
<h3><?php echo __('Edit Executor') ?></h3>

<hr />
<?php echo $this->Form->create('Executors', array('type' => 'post', 'class' => 'edit-form')); ?>

<div class="row">
    <div class="col-md-3">
        <label for="cpf"><?php echo __('CPF') ?></label>
        <div class="form-group">
            <input id="cpf" class="form-control" type="text" name="cpf" maxlength="11" placeholder="CPF" value="<?php echo $executor->cpf ?>" />
        </div>
    </div>

    <div class="col-md-3">
        <label for="email"><?php echo __('Email') ?></label>
        <div class="form-group">
            <input id="email" class="form-control" type="text" name="email" placeholder="Email" value="<?php echo $executor->email ?>" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <label for="name"><?php echo __('Name') ?></label>
        <div class="form-group">
            <input id="name" class="form-control" type="text" name="name" placeholder="Name" value="<?php echo $executor->name ?>" />
        </div>
    </div>
</div>

<input type="hidden" name="category_id" value="1">

<button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>

<div class="clearfix"></div>

<?php echo $this->Form->end() ?>
