
<h3><?php echo __('Executors') ?></h3>

<hr />

<?php if (count($executors->toArray()) > 0) :  ?>

<?php echo $this->element('simple-search-form', [
    'controller' => 'Executors',
    'placeholder' => 'Search by executor name',
    's' => $s,
    'items' => $executors
]) ?>

    
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th scope="col" class="text-uppercase"><?php echo $this->Paginator->sort('cpf') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('name') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('email') ?></th>
            <th scope="col" class="actions"><?php echo __('Actions') ?></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($executors as $executor): ?>
        <tr>
            <td><?php echo h($executor->cpf) ?></td>
            <td><?php echo h($executor->name) ?></td>
            <td><?php echo h($executor->email) ?></td>
            <td class="actions">
                <?php echo $this->Html->link(__(''), ['action' => 'view', $executor->cpf, 'controller' => 'Executors'], ['class' => 'glyphicon glyphicon-eye-open']) ?>
                <?php echo $this->Html->link(__(''), ['action' => 'edit', $executor->cpf, 'controller' => 'Executors'], ['class' => 'glyphicon glyphicon-pencil']) ?>
                <?php echo $this->Form->postLink(
                    __(''), 
                    ['action' => 'delete', $executor->cpf, 'controller' => 'Executors'], 
                    ['confirm' => __('Are you sure you want to delete # {0}?', $executor->cpf), 
                        'class' => 'glyphicon glyphicon-trash']
                ) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('paginator') ?>

<?php else : ?>
<p><?php echo __('You do not have Executors to list') ?>. 
<?php echo $this->Html->link(__('Add Executor'), ['controller' => 'Executors', 'action' => 'add']) ?></p>
<?php endif ?>
