
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>
        <?php echo $title ?> - VICOS Manager
    </title>

    <?php echo $this->Html->meta('icon') ?>

    <?php echo $this->Html->css('bootstrap.min') ?>
    <?php echo $this->Html->css('bootstrap-select.min') ?>
    <?php echo $this->Html->css('datepicker.min') ?>
    <?php echo $this->Html->css('custom') ?>

    <?php echo $this->Html->script('jquery-3.1.1.min') ?>
    <?php echo $this->Html->script('bootstrap.min') ?>
    <?php echo $this->Html->script('bootstrap-select.min') ?>
    <?php echo $this->Html->script('datepicker.min') ?>
</head>
<body>
    <header>
        <div class="container">
            <div class="top">
                <h1>
                    <a href="/">
                        <?php echo $this->Html->image('logo.png', ['class' => 'logo']) ?>
                        VICOS Manager
                    </a> 
                    <small><?php echo $title ?></small>
                </h1>
            </div>

            <ol class="breadcrumb">
                <li class="<?php echo $this->name == 'So' ? 'active' : '' ?>">
                    <?php echo $this->Html->link(__('Services Orders'), ['controller' => 'So', 'action' => 'index']) ?>
                </li>
                <li class="<?php echo $this->name == 'Services' ? 'active' : '' ?>">
                    <?php echo $this->Html->link(__('Services'), ['controller' => 'Services', 'action' => 'index']) ?>
                </li>
                <li class="<?php echo $this->name == 'Executors' ? 'active' : '' ?>">
                    <?php echo $this->Html->link(__('Executors'), ['controller' => 'Executors', 'action' => 'index']) ?>
                </li>
                <li class="<?php echo $this->name == 'Clients' ? 'active' : '' ?>">
                    <?php echo $this->Html->link(__('Clients'), ['controller' => 'Clients', 'action' => 'index']) ?>
                </li>
            </ol>
        </div>
    </header>

    <div class="container clearfix">
        <div class="row">
            <div class="col-md-3">
                <?php echo $this->element($this->name . '/sidebar') ?>
            </div>
            
            <div id="printable" class="col-md-9">
                <?php echo $this->Flash->render() ?>
                <?php echo $this->fetch('content') ?>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <hr />
            <address class="text-center">
                <small>
                    By Leandro de Amorim <br />
                    <a href="mailto:androrim@gmail.com">androrim@gmail.com</a> - 48 99908 7399
                </small>
            </address>
        </div>
    </footer>

    <script>
        $('[data-toggle="datepicker"]').datepicker();

        $('.datepicker-trigger').each(function (i, element) {
            $(element).click(function (event) {
                var name = $(this).data('trigger');
                
                setTimeout(function () {
                    $('input[name="' + name + '"]').click();
                }, 100);
            });
        });
    </script>
</body>
</html>
