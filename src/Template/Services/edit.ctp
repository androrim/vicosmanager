
<h3><?php echo __('Edit Service') ?></h3>

<hr />

<?php echo $this->Form->create('Sesrvice', array('type' => 'pos', 'class' => 'edit-form')); ?>

<div class="row">
    <div class="col-md-9">
        <label for="description"><?php echo __('Description') ?></label>
        <div class="form-group">
            <input id="description" class="form-control" type="text" name="description" value="<?php echo $service->description ?>" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <label for="price"><?php echo __('Price') ?></label>
        <div class="input-group">
            <div class="input-group-addon"><?php echo __('U$') ?></div>
            <input id="price" class="form-control" type="text" name="price" value="<?php echo $service->price ?>" />
        </div>
    </div>
    <div class="col-md-3">
        <label for="deadline"><?php echo __('Deadline') ?></label>
        <div class="form-group">
            <input id="deadline" class="form-control" type="text" name="deadline" value="<?php echo $service->deadline ?>" />
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>

<div class="clearfix"></div>

<?php echo $this->Form->end() ?>
