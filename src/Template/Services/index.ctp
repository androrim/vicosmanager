
<h3><?php echo __('Services') ?></h3>

<hr />

<?php if (count($services->toArray()) > 0) :  ?>

<?php echo $this->element('simple-search-form', [
    'controller' => 'Services',
    'placeholder' => 'Search by service description',
    's' => $s,
    'items' => $services
]) ?>

<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th scope="col"><?php echo $this->Paginator->sort('id') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('description') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('price') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('deadline') ?></th>
            <th scope="col" class="actions"><?php echo __('Actions') ?></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($services as $service): ?>
        <tr>
            <td><?php echo $service->id ?></td>
            <td><?php echo $service->description ?></td>
            <td><?php echo __('U$') ?> <?php echo $service->price ?></td>
            <td><?php echo $service->deadline ?> <?php echo __('Days') ?></td>
            <td class="actions">
            <?php echo $this->Html->link(__(''), ['action' => 'view', $service->id, 'controller' => 'Services'], ['class' => 'glyphicon glyphicon-eye-open']) ?>
            <?php echo $this->Html->link(__(''), ['action' => 'edit', $service->id, 'controller' => 'Services'], ['class' => 'glyphicon glyphicon-pencil']) ?>
            <?php echo $this->Form->postLink(
                __(''), 
                ['action' => 'delete', $service->id, 'controller' => 'Services'], 
                ['confirm' => __('Are you sure you want to delete # {0}?', $service->id), 
                    'class' => 'glyphicon glyphicon-trash']
            ) ?>
        </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('paginator') ?>

<?php else : ?>
<p><?php echo __('You do not have Services to list') ?>. 
<?php echo $this->Html->link(__('Add Service'), ['controller' => 'Services', 'action' => 'add']) ?></p>
<?php endif ?>
