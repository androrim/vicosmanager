
<h3 class="pull-left"><?php echo $service->description ?></h3>

<?php 
echo $this->Form->postLink( 
    __('Delete'), 
    ['action' => 'delete', $service->id],  
    ['confirm' => __('Are you sure you want to delete # {0}?', $service->id), 
        'class' => 'btn btn-danger pull-right']);
?>

<div class="clearfix"></div>

<hr />
             

<table class="vertical-table">
    <tr>
        <th scope="row"><?php echo __('Id') ?></th>
        <td><?php echo $service->id ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Price') ?></th>
        <td><?php echo __('U$') ?> <?php echo $service->price ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Deadline') ?></th>
        <td><?php echo $service->deadline ?> <?php echo __('Days') ?></td>
    </tr>
</table>
