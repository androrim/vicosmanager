<h3><?php echo __('Add Service Order') ?></h3>

<hr />

<?php echo $this->Form->create('So', array('type' => 'pos', 'class' => 'add-form')); ?>

<div class="row">
    <div class="col-md-3">
        <label for="date"><?php echo __('Date') ?></label>
        <div class="input-group">
            <input id="date" class="form-control" data-toggle="datepicker" type="text" name="date" placeholder="Date" />
            <div class="input-group-addon datepicker-trigger" data-trigger="date">
                <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    
    <div class="col-md-3">
        <div><label for="status"><?php echo __('Status') ?></label></div>
        <select id="status" class="selectpicker form-control" name="status">
            <?php foreach ($status as $id => $stat) : ?>
            <option value="<?php echo $id ?>"><?php echo $stat ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div><label for="executor"><?php echo __('Executor') ?></label></div>
        <select id="executor" class="selectpicker form-control" name="executor_id">
            <?php foreach ($executors as $executor) : ?>
            <option value="<?php echo $executor->cpf ?>"><?php echo $executor->name ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="col-md-3">
        <div><label for="client"><?php echo __('Client') ?></label></div>
        <select id="client" class="selectpicker form-control" name="client_id">
            <?php foreach ($clients as $client) : ?>
            <option value="<?php echo $client->cpf ?>"><?php echo $client->name ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div><label for="services"><?php echo __('Services') ?></label></div>
        <select  name="services[_ids][]" multiple="multiple" id="services">
            <?php foreach ($services as $service) : ?>
            <option value="<?php echo $service->id ?>"><?php echo $service->description ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>

<div class="clearfix"></div>

<?php echo $this->Form->end() ?>

<script>
    $('[data-toggle="datepicker"]').datepicker();
</script>