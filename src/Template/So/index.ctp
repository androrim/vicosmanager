
<h3><?php echo __('List Services Orders') ?></h3>

<hr />

<?php if (count($so->toArray()) > 0) :  ?>

<?php echo $this->Form->create('So', array('type' => 'get', 'class' => 'search-form form-group')); ?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <input class="form-control" type="text" name="s" placeholder="Search by service" />
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <input class="form-control teste" data-toggle="datepicker" type="text" name="di" placeholder="Initial Date" />
            <div class="input-group-addon datepicker-trigger" data-trigger="di">
                <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <input class="form-control" data-toggle="datepicker" type="text" name="df" placeholder="Final Date" />
            <div class="input-group-addon datepicker-trigger" data-trigger="df">
                <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <button type="submit" class="btn btn-default navbar-btn">Search</button>
    </div>
</div>
<?php echo $this->Form->end() ?>

<?php if (!is_null($s)) : ?>
<div class="well well-sm">
    <?php echo count($so) ?>
    <?php echo __('Results for') ?> 
    <i>"<?php echo $s; ?>"</i>
    
    <?php echo __('between dates') ?> 
    
    <?php echo date_format($di, 'm/d/Y') ?> 
    <?php echo __('and') ?> 
    <?php echo date_format($df, 'm/d/Y') ?>

    <a href="/so" type="button" class="pull-right btn btn-default btn-xs">
        Clear <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    </a>
</div>
<?php endif ?>

<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th scope="col"><?php echo $this->Paginator->sort('id') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('client_id') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('executor_id') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('status') ?></th>
            <th scope="col"><?php echo $this->Paginator->sort('date') ?></th>
            <th scope="col" class="actions"><?php echo __('Actions') ?></th>
        </tr>
    </thead>
    
    <tbody>
        <?php foreach ($so as $item): ?>
        <?php 
        $item->client = $self->getClient($item);
        $item->executor = $self->getExecutor($item);
        ?>
        <tr>
            <td><?php echo $this->Number->format($item->id) ?></td>
            <td><?php echo $item->has('client') ? $this->Html->link($item->client->name, ['controller' => 'Clients', 'action' => 'view', $item->client->cpf]) : '' ?></td>
            <td><?php echo $item->has('executor') ? $this->Html->link($item->executor->name, ['controller' => 'Executors', 'action' => 'view', $item->executor->cpf]) : '' ?></td>
            <td><?php echo $self->getStatus($item->status) ?></td>
            <td><?php echo date_format($item->date, 'm/d/Y') ?></td>
            <td class="actions">
                <?php echo $this->Html->link(__(''), ['action' => 'view', $item->id], ['class' => 'glyphicon glyphicon-eye-open']) ?>
                <?php if ($item->status == 0) : ?>
                <?php echo $this->Html->link(__(''), ['action' => 'edit', $item->id], ['class' => 'glyphicon glyphicon-pencil']) ?>
                <?php else: ?>
                <span class="disabled"><a class="glyphicon glyphicon-pencil"></a></span>
                <?php endif ?>
                <?php 
                echo $this->Form->postLink( 
                    __(''), 
                    ['action' => 'delete', $item->id],  
                    ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 
                        'class' => 'glyphicon glyphicon-trash']);
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('paginator') ?>

<?php else : ?>
<p><?php echo __('You do not have Services Orders to list') ?>. 
<?php echo $this->Html->link(__('Add Services Order'), ['controller' => 'So', 'action' => 'add']) ?></p>
<?php endif ?>