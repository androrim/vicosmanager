
<h3 class="pull-left"><?php echo __('Service Order') ?> #<?php echo h($so->id) ?></h3>


<?php 
echo $this->Form->postLink( 
    __('Delete'), 
    ['action' => 'delete', $so->id],  
    ['confirm' => __('Are you sure you want to delete # {0}?', $so->id), 
        'class' => 'btn btn-danger pull-right']);
?>

<a href="javascript:window.print()" class="btn-print btn btn-info pull-right">
    <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
</a>

<div class="clearfix"></div>

<hr />
    
<table class="vertical-table">
    <tr>
        <th scope="row"><?php echo __('Executor') ?></th>
        <td><?php echo $so->has('executor') ? $this->Html->link($so->executor->name, ['controller' => 'Executors', 'action' => 'view', $so->executor->cpf]) : '' ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Client') ?></th>
        <td><?php echo $so->has('client') ? $this->Html->link($so->client->name, ['controller' => 'Clients', 'action' => 'view', $so->client->cpf]) : '' ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Status') ?></th>
        <td><?php echo h($status) ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Date') ?></th>
        <td><?php echo date_format($so->date, 'm/d/Y') ?></td>
    </tr>
    <tr>
        <th scope="row"><?php echo __('Total Price') ?></th>
        <td>U$ <var></var></td>
    </tr>
</table>

<h4><?php echo __('Seervices') ?></h4>

<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col"><?php echo __('Description') ?></th>
            <th scope="col"><?php echo __('Price') ?></th>
            <th scope="col"><?php echo __('Deadline') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($so->services as $service) : ?>
        <tr>
            <td><?php echo $service->id ?></td>
            <td><?php echo $service->description ?></td>
            <td>U$ <span class="unprices"><?php echo $service->price ?></span></td>
            <td><?php echo $service->deadline ?></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>

<script>
$(document).ready(function () {
    var totalPrice = 0;

    $('.unprices').each(function (i, price) {
        totalPrice += parseInt($(price).text());
    });

    $('var').text(totalPrice);
});
</script>
