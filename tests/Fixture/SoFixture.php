<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SoFixture
 *
 */
class SoFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'so';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'executor_id' => ['type' => 'string', 'length' => 11, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'client_id' => ['type' => 'string', 'length' => 11, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'executor_key' => ['type' => 'index', 'columns' => ['executor_id'], 'length' => []],
            'client_key' => ['type' => 'index', 'columns' => ['client_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'so_ibfk_1' => ['type' => 'foreign', 'columns' => ['executor_id'], 'references' => ['persons', 'cpf'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'so_ibfk_2' => ['type' => 'foreign', 'columns' => ['client_id'], 'references' => ['persons', 'cpf'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'executor_id' => 'Lorem ips',
            'client_id' => 'Lorem ips',
            'status' => 1,
            'date' => '2016-12-14 04:16:01'
        ],
    ];
}
