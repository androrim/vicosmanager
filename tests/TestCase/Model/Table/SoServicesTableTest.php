<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SoServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SoServicesTable Test Case
 */
class SoServicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SoServicesTable
     */
    public $SoServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.so_services',
        'app.so',
        'app.persons',
        'app.categories',
        'app.services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SoServices') ? [] : ['className' => 'App\Model\Table\SoServicesTable'];
        $this->SoServices = TableRegistry::get('SoServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SoServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
