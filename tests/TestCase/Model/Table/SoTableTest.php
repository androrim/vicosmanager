<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SoTable Test Case
 */
class SoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SoTable
     */
    public $So;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.so',
        'app.persons',
        'app.categories',
        'app.services',
        'app.so_services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('So') ? [] : ['className' => 'App\Model\Table\SoTable'];
        $this->So = TableRegistry::get('So', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->So);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
